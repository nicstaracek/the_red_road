﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndFade : MonoBehaviour {

    public CanvasGroup blackFade;
    public CanvasGroup blackFadeEnd;

    public bool startFade = false;
    public bool introFade = false;

    // Use this for initialization
    void Start()
    {
        introFade = true;

        blackFade.alpha = 1;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.T))
        //{
        //    Debug.Log("Pressing");
        //    startFade = true;
        //}
        if (introFade)
        {
            blackFade.alpha -= 0.0007f;
        }

        if (startFade)
        {
            blackFadeEnd.alpha += 0.003f;

            if (blackFadeEnd.alpha >= 1)
            {
                Debug.Log("Has Quit");
                Application.Quit();
            }
        }
    }

    public void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.tag == "Car")
        {
            startFade = true;
        }
    }
}
