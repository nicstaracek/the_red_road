﻿using UnityEngine;
using System.Collections;

public class HeadlampController : MonoBehaviour
{
    static float time = 0.0f;
    public float min = 4.5f;
    public float max = 6.5f;

    public Light headLamp_1;
    public Light headLamp_2;

    public float lightRangeMin = 10.0f;
    public float lightRangeMax = 20.0f;
    public float lightIntensityMin = 5.0f;
    public float lightIntensityMax = 7.0f;

    public CameraControl cameraControl;

    // Use this for initialization
    void Start()
    {
        cameraControl = GameObject.FindGameObjectWithTag("CameraControl").GetComponent<CameraControl>();
        headLamp_1 = GameObject.FindGameObjectWithTag("Headlamp_1").GetComponent<Light>();
        headLamp_2 = GameObject.FindGameObjectWithTag("Headlamp_2").GetComponent<Light>();
    }

    // Update is called once per frame
    void Update()
    {
        //time += 0.5f * Time.deltaTime;

        if (Input.GetButton("Fire1"))
        {
            time += 0.5f * Time.deltaTime;
            cameraControl.m_MinSize = Mathf.Lerp(max, min, time);    
            headLamp_1.range = Mathf.Lerp(lightRangeMin, lightRangeMax, time);
            headLamp_2.range = Mathf.Lerp(lightRangeMin, lightRangeMax, time);
            headLamp_1.intensity = Mathf.Lerp(lightIntensityMin, lightIntensityMax, time);
            headLamp_2.intensity = Mathf.Lerp(lightIntensityMin, lightIntensityMax, time);
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            cameraControl.m_MinSize = 6.5f;
            headLamp_1.range = 10.0f;
            headLamp_2.range = 10.0f;
            headLamp_1.intensity = 5.0f;
            headLamp_2.intensity = 5.0f;
            time = 0.0f;
        }
        
        //if (cameraControl.m_MinSize == 4.5f)
        //{
        //    cameraControl.m_MinSize = Mathf.Lerp(max, min, time);
            
            //headLamp_1.range = 10.0f;
            //headLamp_2.range = 10.0f;
            //headLamp_1.intensity = 5.0f;
            //headLamp_2.intensity = 5.0f;
        } 
    }
