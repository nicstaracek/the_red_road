﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MenuUIManager : MonoBehaviour
{
    public Button startText;
    //public Button controlsText;
    //public Button creditsText;
    //public Button exitText;

    // Use this for initialization
    void Start()
    {
        startText = startText.GetComponent<Button>();
        //controlsText = controlsText.GetComponent<Button>();
        //creditsText = creditsText.GetComponent<Button>();
        //exitText = exitText.GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartPress()
    {
        SceneManager.LoadScene(1);
    }
    //public void ControlsPress()
    //{
    //    SceneManager.LoadScene(2);
    //}
    //public void CreditsScene()
    //{
    //    SceneManager.LoadScene(3);
    //}
    //public void ExitPress()
    //{
    //    Application.Quit();
    //}
}